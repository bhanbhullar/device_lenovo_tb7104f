# Include layers
DEVICE_PACKAGE_OVERLAYS += device/lennovo/TB7104F/overlay

# Define screen size for prebuilt apps
PRODUCT_AAPT_CONFIG := large xlarge mdpi tvdpi hdpi
PRODUCT_AAPT_PREF_CONFIG := mdpi

# Use dtbhtoolExynos to build dt.img
PRODUCT_PACKAGES += \
    dtbhtoolExynos

# Screen size for boot animation
TARGET_SCREEN_HEIGHT := 1024
TARGET_SCREEN_WIDTH := 600
