# Inherit device configuration
$(call inherit-product, device/lenovo/TB7104F/device.mk)

# Inherit from the common Open Source product configuration
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base_telephony.mk)

# Inherit common CM phone.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Set those variables here to overwrite the inherited values.
PRODUCT_NAME := lineage_TB7104F
PRODUCT_DEVICE := TB7104F
PRODUCT_BRAND := lenovo
PRODUCT_MANUFACTURER := lenovo
