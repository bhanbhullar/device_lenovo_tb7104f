set -e

DEVICE=TB7104F
VENDOR=lenovo

# Load extract_utils and do some sanity checks
MY_DIR="${BASH_SOURCE%/*}"
if [[ ! -d "${MY_DIR}" ]]; then MY_DIR="${PWD}"; fi

LINEAGE_ROOT="${MY_DIR}"/../../..

HELPER="${LINEAGE_ROOT}/vendor/lineage/build/tools/extract_utils.sh"
if [ ! -f "${HELPER}" ]; then
    echo "Unable to find helper script at ${HELPER}"
    exit 1
fi
source "${HELPER}"

function blob_fixup() {
    case "${1}" in
    esac
}


if [ -z "${SRC}" ]; then
    SRC="adb"
fi


# Initialize the helper for common device
setup_vendor "${DEVICE}" "${VENDOR}" "${LINEAGE_ROOT}" true

extract "${MY_DIR}/proprietary-files.txt" "${SRC}" \

"${MY_DIR}/setup-makefiles.sh"

